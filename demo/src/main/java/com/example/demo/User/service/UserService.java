package com.example.demo.User.service;

import com.example.demo.User.dto.UserDto;

import java.util.List;
import java.util.Optional;

public interface UserService {
    UserDto saveCustomer(UserDto customerNew);

    List<UserDto> getUsers();

    Optional<UserDto> getUserById(Long id);

    String deleteUserById(Long id);

    String updateUser(UserDto user);
}
