package com.example.demo.User.service.impl;


import com.example.demo.User.dto.UserDto;
import com.example.demo.User.repository.UserRepository;
import com.example.demo.User.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;



    @Override
    public UserDto saveCustomer(UserDto user) {
        if (user != null) {
            return userRepository.save(user);
        }

        return new UserDto();
    }

    @Override
    public List<UserDto> getUsers(){
        return userRepository.findAll();
    }

    @Override
    public Optional<UserDto> getUserById(Long id){
        return userRepository.findById(id);
    }

    @Override
    public String deleteUserById(Long id){
        if(userRepository.findById(id).isPresent()){

            userRepository.deleteById(id);
            return "se elimino";
        }
        return "error eliminando";
    }


    @Override
    public String updateUser(UserDto user){

        if(userRepository.findById(user.getId()).isPresent()){
            userRepository.save(user);

            return "se ha actualizado";

        }

        return "error actualizando";

    }



}
